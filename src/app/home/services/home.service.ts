import { Injectable } from '@angular/core';
import { TopMenu, Channel, ImageSlider, Ad, Product } from 'src/app/shared';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(private http: HttpClient) {}
  // topMenus: TopMenu[] = [
  //   {
  //     title: 'Hot',
  //     link: 'hot',
  //     id: 1
  //   },
  //   {
  //     id: 2,
  //     title: 'Men',
  //     link: 'men'
  //   },
  //   {
  //     id: 3,
  //     title: 'Department',
  //     link: 'department'
  //   },
  //   {
  //     id: 4,
  //     title: 'Sports',
  //     link: 'sports'
  //   },
  //   {
  //     id: 5,
  //     title: 'Mobile',
  //     link: 'mobile'
  //   },
  //   {
  //     id: 6,
  //     title: 'Textile',
  //     link: 'textile'
  //   },
  //   {
  //     id: 7,
  //     title: 'Food',
  //     link: 'food'
  //   },
  //   {
  //     id: 8,
  //     title: 'Appliance',
  //     link: 'appliance'
  //   },
  //   {
  //     id: 9,
  //     title: 'Shoes',
  //     link: 'shoes'
  //   },
  //   {
  //     id: 10,
  //     title: 'Cars',
  //     link: 'cars'
  //   },
  //   {
  //     id: 11,
  //     title: 'Fruits',
  //     link: 'fruits'
  //   },
  //   {
  //     id: 12,
  //     title: 'Computers',
  //     link: 'computers'
  //   },
  //   {
  //     id: 13,
  //     title: 'Underwears',
  //     link: 'underwears'
  //   },
  //   {
  //     id: 14,
  //     title: 'Home',
  //     link: 'home'
  //   },
  //   {
  //     id: 15,
  //     title: 'Baby',
  //     link: 'baby'
  //   },
  //   {
  //     id: 16,
  //     title: 'Makeup',
  //     link: 'makeup'
  //   },
  //   {
  //     id: 17,
  //     title: 'Furnitures',
  //     link: 'furnitures'
  //   }
  // ];
  // imageSliders: ImageSlider[] = [
  //   {
  //     imgUrl:
  //       'https://media.istockphoto.com/photos/morning-jogging-picture-id497687118',
  //     link: '',
  //     caption: ''
  //   },
  //   {
  //     imgUrl:
  //       'https://media.istockphoto.com/photos/listening-the-music-picture-id508949258',
  //     link: '',
  //     caption: ''
  //   },
  //   {
  //     imgUrl:
  //       'https://media.istockphoto.com/photos/pretty-young-teenage-girl-relaxing-on-a-grass-picture-id521982322',
  //     link: '',
  //     caption: ''
  //   },
  //   {
  //     imgUrl:
  //       'https://media.istockphoto.com/photos/beautiful-women-working-out-in-gym-picture-id623680490',
  //     link: '',
  //     caption: ''
  //   },
  //   {
  //     imgUrl:
  //       'https://media.istockphoto.com/photos/jogging-with-my-best-friend-picture-id850045040',
  //     link: '',
  //     caption: ''
  //   }
  // ];

  // channels: Channel[] = [
  //   {
  //     id: 1,
  //     title: 'Limited',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-11-13/298376dd8176f90df743de9f08a756eb.png',
  //     link: 'hot'
  //   },
  //   {
  //     id: 2,
  //     title: 'Clearance',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-12-07/678088ee2500f08a193ea8619bc0ae76.png',
  //     link: 'men'
  //   },
  //   {
  //     id: 3,
  //     title: 'Brands',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2019-03-14/a01571d7bde632029c76ad9a298570ec.png',
  //     link: 'sports'
  //   },
  //   {
  //     id: 4,
  //     title: 'Fruits',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-12-03/d21a7598e29ce189a9a57bb234ee4fad.png',
  //     link: 'department'
  //   },
  //   {
  //     id: 5,
  //     title: 'Promo',
  //     icon:
  //       'http://t01img.yangkeduo.com/images/2018-05-16/d86ceaeaa0bccaeb3b3dee76f64f0192.png',
  //     link: 'food'
  //   },
  //   {
  //     id: 6,
  //     title: 'Free',
  //     icon:
  //       'http://t05img.yangkeduo.com/images/2018-05-16/bf18833fa4c298a040fe01f279f6f6ec.png',
  //     link: 'textile'
  //   },
  //   {
  //     id: 7,
  //     title: 'Cash',
  //     icon:
  //       'http://t10img.yangkeduo.com/images/2018-05-16/712fc1e7f4f7b0572257ef162b5185a9.png',
  //     link: 'mobile'
  //   },
  //   {
  //     id: 8,
  //     title: 'Lottery',
  //     icon:
  //       'http://t05img.yangkeduo.com/images/2018-05-04/c71c9acd8b48203a704f6c0951caddc0.png',
  //     link: 'appliance'
  //   },
  //   {
  //     id: 9,
  //     title: 'Recharge',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-12-02/68aefda33f6afac045997edd25a3844e.png',
  //     link: 'shoes'
  //   },
  //   {
  //     id: 10,
  //     title: 'Shops',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2019-01-20/d36b7af30da354cb2c8ad4ea7bd819cd.png',
  //     link: 'computers'
  //   },
  //   {
  //     id: 11,
  //     title: 'Sign In',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-08-01/f13e2dff54d604518a1db4facd89d300.png',
  //     link: 'fruits'
  //   },
  //   {
  //     id: 12,
  //     title: 'Golden Pig',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2019-02-05/1351e256a0319a6885761b937d06d581.png',
  //     link: 'cars'
  //   },
  //   {
  //     id: 13,
  //     title: 'Electronics',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-11-26/ee327a5ee7fb7ff76a98cf71be967a20.png',
  //     link: 'underwears'
  //   },
  //   {
  //     id: 14,
  //     title: 'Shopping',
  //     icon:
  //       'http://t03img.yangkeduo.com/images/2018-05-16/a666ac01e718dd06231a722baa6fa935.png',
  //     link: 'home'
  //   },
  //   {
  //     id: 15,
  //     title: 'Bargain',
  //     icon:
  //       'http://t00img.yangkeduo.com/goods/images/2018-11-14/4ad66f8d7d28d6237a9f25b9a6d19f3e.png',
  //     link: 'baby'
  //   },
  //   {
  //     id: 16,
  //     title: 'Free',
  //     icon:
  //       'http://t11img.yangkeduo.com/images/2018-04-28/5cfc9925dac860135143921e0f687a7d.png',
  //     link: 'furnitures'
  //   }
  // ];
  // getTabs() {
  //   return this.topMenus;
  // }
  // getChannels() {
  //   return this.channels;
  // }
  // getBanners() {
  //   return this.imageSliders;
  // }
  getBanners() {
    return this.http.get<ImageSlider[]>(`${environment.baseUrl}/banners`);
  }
  getTabs() {
    return this.http.get<TopMenu[]>(`${environment.baseUrl}/tabs`);
  }
  getChannels() {
    return this.http.get<Channel[]>(`${environment.baseUrl}/channels`);
  }
  getAdByTab(tab: string) {
    return this.http.get<Ad[]>(`${environment.baseUrl}/ads`, {
      params: {categories_like: tab}
    });
  }
  getProductsByTab(tab: string) {
    return this.http.get<Product[]>(`${environment.baseUrl}/products`, {
      params: {categories_like: tab}
    });
  }

}
