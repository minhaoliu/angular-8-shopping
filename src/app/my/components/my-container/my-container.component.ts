import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-my-container',
  templateUrl: './my-container.component.html',
  styleUrls: ['./my-container.component.css']
})
export class MyContainerComponent implements OnInit {
  isVisible = false;
  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
    this.validateForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      remember: [true]
    });
  }

  ngOnInit() {

  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  submitForm(f: NgForm): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.valid) {
      this.http.post('http://globalportal.test/login',
      {
        email: f.controls['email'].value,
        password: f.controls['password'].value,
      })
      .subscribe(data => {
        if (data === true) {
          this.router.navigate(['/']);
        } else {
          alert('Incorrect username or password');
        }
      });
  }
}

}
