import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { TopMenu } from 'src/app/shared/components';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService, token } from '../../services';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-home-container',
  templateUrl: './home-container.component.html',
  styleUrls: ['./home-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeContainerComponent implements OnInit {

  constructor(
      private router: Router,
      private service: HomeService,
      private route: ActivatedRoute,
      // `@Inject` 这个注解用于找到可注入的标识，
      // 也就是 provide 的那个标识
      @Inject(token) private baseUrl: string
    ) {}
  // topMenus: TopMenu[] = [];
  topMenus$: Observable<TopMenu[]>;
  selectedTabLink$: Observable<string>;

  ngOnInit() {
    // this.topMenus = this.service.getTabs();
    // this.service.getTabs().subscribe(tabs => { this.topMenus = tabs; });
    this.topMenus$ = this.service.getTabs();
    this.selectedTabLink$ = this.route.firstChild.paramMap.pipe(
      filter(params => params.has('tabLink')),
      map(params => params.get('tabLink'))
    );
  }


  handleTabSelected(topMenu: TopMenu) {
    this.router.navigate(['home', topMenu.link]);
  }

}
