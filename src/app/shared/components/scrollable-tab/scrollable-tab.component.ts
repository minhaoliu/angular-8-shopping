import { Component, OnInit, Input, Output, ChangeDetectionStrategy } from '@angular/core';
import { EventEmitter } from '@angular/core';

export interface TopMenu {
  title: string;
  link: string;
  id: number;
}

@Component({
  selector: 'app-scrollable-tab',
  templateUrl: './scrollable-tab.component.html',
  styleUrls: ['./scrollable-tab.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScrollableTabComponent {
  @Input() selectedTabLink: string;
  @Input() menus: TopMenu[] = [];
  @Input() backgroundColor = '#fff';
  @Input() titleActiveColor = 'yellow';
  @Input() titleColor = 'blue';
  @Input() indicatorColor = 'brown';
  @Output() tabSelected = new EventEmitter();

  constructor() { }

  handleSelection(index: number) {
    this.tabSelected.emit(this.menus[index]);
  }
}
