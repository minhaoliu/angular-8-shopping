import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { OrderService } from '../..';
import { DialogService } from 'src/app/dialog';
import { Observable } from 'rxjs';

declare var paypal;

@Component({
  selector: 'app-payme',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  totalPrice$: Observable<number>;

  product = {
    price: 40.15,
    description: 'some product'
  };

  paidFor = false;


  constructor(
    private orderService: OrderService,
    private dialogService: DialogService,
    ) { }

  ngOnInit() {
    paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: this.product.description,
                amount: {
                  currency_code: 'USD',
                  value: this.product.price
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          this.paidFor = true;
          console.log(order);
        },
        onError: err => {
          console.log(err);
        }
      })
      .render(this.paypalElement.nativeElement);
  }
}
