import { Component, OnInit, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { DialogService } from 'src/app/dialog';
import { Observable, Subject, combineLatest, merge } from 'rxjs';
import { ProductVariant } from '../../domain';
import { map, tap, shareReplay } from 'rxjs/operators';
import { Payment } from '../payment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmOrderComponent implements OnInit {
  item$: Observable<object | null>;
  count$ = new Subject<number>();
  totalPrice$: Observable<number>;
  payments: Payment[];
  temp = new EventEmitter();
  constructor(
    private dialogService: DialogService,
    private router: Router
    ) { }

  ngOnInit() {
    this.payments = this.payments = [
      {
        id: 1,
        name: 'WeChat Pay',
        icon: 'assets/icons/wechat_pay.png',
        desc: 'Save $1'
      },
      {
        id: 2,
        name: 'AliPay',
        icon: 'assets/icons/alipay.png'
      },
      {
        id: 3,
        name: 'Paypal',
        icon: 'assets/icons/paypal.jpg'
      }
    ];
    this.item$ = this.dialogService.getData().pipe(
      tap(val => console.log(val)),
      shareReplay(1)
    );
    const unitPrice$ = this.item$.pipe(
      map(
        (item: {variant: ProductVariant; count: number}) => item.variant.price
      )
    );
    const amount$ = this.item$.pipe(
      map((item: {variant: ProductVariant; count: number}) => item.count)
    );
    const mergedCount$ = merge(amount$, this.count$);
    this.totalPrice$ = combineLatest([unitPrice$, mergedCount$]).pipe(
      map(([price, amount]) => price * amount)
    );
  }

  handleAmountChange(count: number) {
    this.count$.next(count);
  }

  handlePay() {
    this.router.navigate(['/orders/paypal']);
  }
}
